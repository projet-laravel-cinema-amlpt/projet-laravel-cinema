<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $martine=User::create([
          'name' => 'Martine',
          'email' => 'martine@martine.com',
          'password' => Hash::make('missmartine')
        ]);
        $martine->roles()->attach(1);

        $mike=User::create([
          'name' => 'Mike',
          'email' => 'mike@mike.com',
          'password' => Hash::make('mikedrop')
        ]);
        $mike->roles()->attach(2);
    }
};
