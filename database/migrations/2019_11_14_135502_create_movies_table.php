<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('title', 255)->unique();
            $table->longText('resume');
            $table->integer('year');
            $table->string('released', 11);
            $table->string('runtime', 8);
            $table->string('genre', 255);
            $table->string('director', 255);
            $table->string('language', 255);
            $table->string('country', 255);
            $table->string('awards', 255);
            $table->longText('poster');
            $table->string('Type', 255);
            $table->string('DVD', 11);
            $table->string('Production', 255);
            $table->boolean('avalaible');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
