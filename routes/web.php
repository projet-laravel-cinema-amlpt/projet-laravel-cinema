<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Controllers\HomeController;

Route::get('/', "HomeController@home");

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// Views :
Route::get('/accueil', function() {
    return view('accueil');
});

// Route::get('/panier', function() {
//     return view('panier');
// });
Route::get('/panier', 'PanierController@listPanier');

Route::get('/films', 'FilmsController@filmsList');

Route::get('/modif-film', function() {
    return view('modif-film');
})->middleware('checkadmin');

Route::resource('movie', 'MovieController');

Route::get('/recup-film', 'AjoutFilmsController@rechercheApi');
Route::get('/ajout-film', 'AjoutFilmsController@create');
Route::post('/ajout-film/add', 'AjoutFilmsController@ajoutFilm');

Route::get('/users-list', 'UsersListController@usersList');

Route::get('/denied', function(){
  return view('not-allowed');
});