@extends('layouts.app')

@section('content')
    <h1>Liste des utilisateurs</h1>
    <br>
        <table border="1">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Mot de passe</th>
                    <th>Supprimer l'utilisateur</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td> {{ $user->name }} </td>
                        <td> {{ $user->email }} </td>
                        <td> {{ $user->roles->first()->slug }} </td>
                        <td> {{ $user->password }} </td>
                        <td><button> supprimer </button> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
@endsection