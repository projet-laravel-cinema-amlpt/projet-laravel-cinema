<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
            @if(!Auth::guest())
              <h1>Ajouter film</h1>
  <h2>Accès reservé admin</h2>
  <br>
  <form method="GET" action="">
      <input type="search" id="search" name="search" value="...">
      <input type="submit" value="Rechercher">
  </form>  

  <br> 

  <?php
      if(isset($_GET['search'])) {
      $homepage = file_get_contents('http://www.omdbapi.com/?apikey=6bb2a3cf&t='.$_GET['search']);
      $homepage = json_decode($homepage);
      // var_dump($homepage);
      }
  ?>
  
  <table border="1">
      <thead>
          <tr>
              <th>Titre</th>
              <th>Resume</th>
              <th>Date de sortie</th>
              <th>Durée</th>
              <th>Réalisateur</th>
              <th>Affiche</th>
              <th>Disponible</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td> {{ $homepage->Title ?? ''}} </td>
              <td> {{ $homepage->Plot ?? ''}} </td>
              <td> {{ $homepage->Released ?? ''}} </td>
              <td> {{ $homepage->Runtime ?? ''}} </td>
              <td> {{ $homepage->Director ?? ''}} </td>
              <td><img src="{{ $homepage->Poster ?? ''}}" alt="affiche" class="affiche"></td>
              <td> {{ $homepage->Response ?? ''}} </td>                    
          </tr>
      </tbody>
  </table>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
                @endif
            </div>
        </div>
    </body>
</html>
