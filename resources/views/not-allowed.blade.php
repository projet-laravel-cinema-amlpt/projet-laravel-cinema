@extends('layouts.app')

@section('content')
  <div class="refused">
    <p>Désolé, vous n'êtes pas autorisé à accéder à cette page.</p>
  </div>
@endsection