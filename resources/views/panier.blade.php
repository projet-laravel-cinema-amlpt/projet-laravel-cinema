@extends('layouts.app')

@section('content')
    <h1>Test de la page panier</h1>
    <h2>Acces reservé utilisateur</h2>
    <br>

    <table border="1">
        <thead>
            <tr>
                <th>Affiche</th>
                <th>Titre</th>
                <th>Annuler</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><img src="{{ $movie->poster ?? ''}}" alt="affiche" class="affiche"></td>
                <td>{{ $movie->title ?? ''}}</td>
                <td><button>Annuler</button></td>
            </tr>
        </tbody>
    </table>



@endsection