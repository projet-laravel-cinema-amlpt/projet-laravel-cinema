@extends('layouts.app')

@section('content')
    <h1>Liste des films</h1>
    <h2>Accès reservé utilisateur</h2>
    <br>
        <table border="1">
            <thead>
                <tr>
                    <th>Titre</th>
                    <th>Resume</th>
                    <th>Date de sortie</th>
                    <th>Durée</th>
                    <th>Réalisateur</th>
                    <th>Affiche</th>
                    <th>Disponible</th>
                    <th>Ajouter au panier</th>
                    <th>Rendre le film</th>
                    @if(Auth::user()->roles->first()->slug == 'administrateur')
                        <th>Modifier le film</th>
                        <th>Supprimer le film</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($movies as $movie)
                    <tr>
                        <td> {{ $movie->title }} </td>
                        <td> {{ $movie->resume }} </td>
                        <td> {{ $movie->released }} </td>
                        <td> {{ $movie->runtime }} </td>
                        <td> {{ $movie->director }} </td>
                        <td><img src="{{ $movie->poster }}" alt="affiche" class="affiche"></td>
                        <td> {{ $movie->avalaible }} </td>
                        <td><button> ajouter au panier </button></td>
                        <td><button> rendre </button></td>
                        @if(Auth::user()->roles->first()->slug == 'administrateur')
                            <td><button> modifier </button></td>
                            <td><button> supprimer </button></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
@endsection