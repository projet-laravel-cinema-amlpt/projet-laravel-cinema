@extends('layouts.app')

@section('content')
    <h1>Ajouter film</h1>
    <h2>Accès reservé admin</h2>
    <br>
    <form method="GET" action="/recup-film">
        <input type="search" id="search" name="search" value="" placeholder="un titre de film en anglais et avec des - à la place des espaces, merci bisous partout:">
        <input type="submit" value="Rechercher">
    </form>  

    <br>

    <table border="1">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Resume</th>
                <th>Date de sortie</th>
                <th>Durée</th>
                <th>Réalisateur</th>
                <th>Affiche</th>
                <th>Add</th>
            </tr>
        </thead>
        <tbody>
            <form action="">
                <tr>
                    <td> {{ $homepage->Title ?? ''}} </td>
                    <td> {{ $homepage->Plot ?? ''}} </td>
                    <td> {{ $homepage->Released ?? ''}} </td>
                    <td> {{ $homepage->Runtime ?? ''}} </td>
                    <td> {{ $homepage->Director ?? ''}} </td>
                    <td><img src="{{ $homepage->Poster ?? ''}}" alt="affiche" class="affiche"></td>                  
                </tr>
            </form>
        </tbody>
    </table>

    <br>
    @if(isset($homepage))
    <form method="post" action="/ajout-film/add">
        @csrf
        <input type="text"  name="title" value="{{ $homepage->Title ?? ''}} ">
        <input type="text"  name="resume" value="{{ $homepage->Plot ?? ''}}">
        <input type="text"  name="released" value="{{ $homepage->Released ?? ''}}">
        <input type="text"  name="runtime" value="{{ $homepage->Runtime ?? ''}}">
        <input type="text" name="director" value="{{ $homepage->Director ?? ''}}">
        <input type="text" name="poster" value="{{$homepage->Poster ?? ''}}">                 
        <input type="text" name="year" value="{{$homepage->Year ?? ''}}">                 
        <input type="text" name="genre" value="{{$homepage->Genre ?? ''}}">                 
        <input type="text" name="language" value="{{$homepage->Language ?? ''}}">                 
        <input type="text" name="country" value="{{$homepage->Country ?? ''}}">                 
        <input type="text" name="awards" value="{{$homepage->Awards ?? ''}}">                 
        <input type="text" name="Type" value="{{$homepage->Type ?? ''}}">                 
        <input type="text" name="DVD" value="{{$homepage->DVD ?? ''}}">                 
        <input type="text" name="Production" value="{{$homepage->Production ?? ''}}">                 
        <button type="submit">Submit</button>               
    </form>
    @endif
@endsection