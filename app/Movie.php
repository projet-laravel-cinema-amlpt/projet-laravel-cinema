<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','resume','year','released','runtime','genre','director','language','country','awards','poster','Type','DVD','Production','avalaible',
    ];

    // Relations :
    public function orders() {
        return $this->belongsToMany('app\Order');
    }
}
