<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference',
    ];

    // Relations :
    public function movies() {
        return $this->belongsToMany('app\Movie');
    }
    public function users() {
        return $this->belongsTo('app\User');
    }
}
