<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Support\Facades\DB;

class AjoutFilmsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkadmin');
    }
    public function create(){
        return view('ajout-film');
    }

    public function rechercheApi(Request $request) {
        $homepage = file_get_contents('http://www.omdbapi.com/?apikey=6bb2a3cf&t='.$request->search);
        $homepage = json_decode($homepage);
        return view('ajout-film', compact('homepage'));
    }

    public function ajoutFilm(Request $request, Movie $movie) {
        $movie->create([
            'title' => $request->title,
            'resume' => $request->resume,
            'year' => $request->year,
            'released' => $request->released,
            'runtime' => $request->runtime,
            'genre' => $request->genre,
            'director' => $request->director,
            'language' => $request->language,
            'country' => $request->country,
            'awards' => $request->awards,
            'poster' => $request->poster,
            'Type' => $request->Type,
            'DVD' => $request->DVD,
            'Production' => $request->Production,
            'avalaible' => 1
        ])->save();
        
        return redirect('films');
        
    }
}