<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;

class UsersListController extends Controller {
    public function usersList() {
        $users = User::all();
        return view('users-list', compact('users'));
    }

    // public function roleUser() {
    //     $roles = DB::table('roles')->get();
    //     foreach ($roles as $role) {
    //         echo($role->slug);
    //     }
    // }

}
