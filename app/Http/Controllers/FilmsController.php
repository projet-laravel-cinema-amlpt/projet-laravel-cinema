<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\User;

class FilmsController extends Controller {
    public function filmsList() {
        $movies = Movie::all();
        return view('films', compact('movies'));
    }
}
